const createNewChild = () => {
    const item = document.getElementById('item');
    const div1 = document.createElement("div");
    div1.innerHTML = "New Element";
    if (item.children.length < 10) {
        item.appendChild(div1);
    } else {
        while(item.lastChild) {
            item.removeChild(item.lastChild)
        }
    }
};