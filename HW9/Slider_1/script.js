let btn_next = document.querySelector(".btn_next");
let btn_prew = document.querySelector(".btn_prew");
let images = ['img/car_1.jpg', 'img/car_2.jpg', 'img/car_3.jpg', 'img/car_4.jpg', 'img/car_5.jpg', 'img/car_6.jpg'];
let i = 0;

btn_prew.addEventListener("click", function () { 
    var slidesImage = document.getElementById("slides");
    i--;
    if (i < 0) i = images.length - 1;
    slidesImage.src = images[i];
})

btn_next.addEventListener("click", function () {
    var slidesImage = document.getElementById("slides");
    i++;
    if (i == images.length) i = 0;
    slidesImage.src = images[i];
})